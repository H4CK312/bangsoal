
package bangsoal.shared;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import static java.util.logging.Level.SEVERE;
import java.util.logging.Logger;
import static java.util.logging.Logger.getLogger;

/**
 *
 * @author Tommy
 */
public class Common {

    private static final Logger LOG = getLogger(Common.class.getName());

    /**
     *
     * @param location
     * @return
     */
    public static Object fileToObject(String location) {
        FileInputStream fin = null;
        ObjectInputStream ois = null;
        try {
            fin = new FileInputStream(location);
            ois = new ObjectInputStream(fin);
            return ois.readObject();
        } catch (IOException | ClassNotFoundException ex) {
            getLogger(Common.class.getName()).log(SEVERE, null, ex);
        } finally {
            try {
                ois.close();
                fin.close();

            } catch (IOException ex) {
                getLogger(Common.class.getName()).log(SEVERE, null, ex);
            }
        }
        return null;
    }

    /**
     *
     * @param file
     * @param o
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static void objectToFile(String file, Object o) throws FileNotFoundException, IOException {
        FileOutputStream fout = null;
        ObjectOutputStream oos = null;

        fout = new FileOutputStream(file);
        oos = new ObjectOutputStream(fout);
        oos.writeObject(o);

        oos.close();
        fout.close();

    }

    private Common() {
    }

}
