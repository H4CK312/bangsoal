
package bangsoal.dao;

import bangsoal.dao.specs.SubjectDao;
import bangsoal.entity.Subject;
import static bangsoal.shared.Common.fileToObject;
import static bangsoal.shared.Common.objectToFile;
import bangsoal.shared.Configuration;
import static bangsoal.shared.Configuration.getInstance;
import java.io.File;
import java.io.IOException;
import static java.util.Arrays.asList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import static java.util.logging.Level.SEVERE;
import java.util.logging.Logger;
import static java.util.logging.Logger.getLogger;

/**
 *
 * @author radityopw
 */
public class SubjectDaoFile implements SubjectDao {

    private static final Logger LOG = getLogger(SubjectDaoFile.class.getName());

    private final Configuration conf = getInstance();
    private final Map<String,Subject> list = new TreeMap<>();

    /**
     *
     * @param a
     */
    @Override
    public void save(Subject a) {

        File dir = new File(conf.getWorkingDir(), "subjects");
        File file = new File(dir, a.getId() + ".data");
        try {
            objectToFile(file.getAbsolutePath(), a);
            list.put(a.getId()+"", a);
        } catch (IOException ex) {
            getLogger(SubjectDaoFile.class.getName()).log(SEVERE, null, ex);
        }

    }

    /**
     *
     * @return
     */
    @Override
    public Collection<Subject> getAll() {

        if (list.isEmpty()) {
            File dir = new File(conf.getWorkingDir(), "subjects");

            List<String> subDir = asList(dir.list());

            subDir.stream().map((d) -> new File(dir, d)).map((ds) -> (Subject) fileToObject(ds.getAbsolutePath())).forEach((s) -> {
                list.put(s.getId()+"", s);
            });
        }

        return list.values();
    }

    /**
     *
     * @param a
     * @return
     */
    @Override
    public Subject get(int a) {
        
        Subject s = null;
        
        if (list.isEmpty()) {

            getAll();

        }
        
        Iterator<Subject> iterator = list.values().iterator();
        
        int index = -1;
        
        while(index < a){
            s = iterator.next();
            index++;
        }
        
        return s;
    }

    /**
     *
     * @param a
     */
    @Override
    public void delete(Subject a) {
        File dir = new File(conf.getWorkingDir(), "subjects");
        File file = new File(dir, a.getId() + ".data");
        if(file.exists()){
            file.delete();
            list.remove(a.getId()+"");
            a = null;
        }
    }

}
