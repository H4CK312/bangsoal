
package bangsoal.dao;

import bangsoal.dao.specs.ConceptDao;
import bangsoal.entity.Concept;
import static bangsoal.shared.Common.fileToObject;
import static bangsoal.shared.Common.objectToFile;
import bangsoal.shared.Configuration;
import static bangsoal.shared.Configuration.getInstance;
import java.io.File;
import java.io.IOException;
import static java.util.Arrays.asList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import static java.util.logging.Level.SEVERE;
import java.util.logging.Logger;
import static java.util.logging.Logger.getLogger;

/**
 *
 * @author radityopw
 */
public class ConceptDaoFile implements ConceptDao {

    private static final Logger LOG = getLogger(ConceptDaoFile.class.getName());

    private final Map<String, Concept> list;

    private final Configuration conf = getInstance();

    private final long subjectId;

    /**
     *
     * @param subjectId
     */
    public ConceptDaoFile(long subjectId) {
        this.subjectId = subjectId;

        list = new TreeMap<>();
    }

    /**
     *
     * @param a
     */
    @Override
    public void save(Concept a) {
        File dir = new File(conf.getWorkingDir(), "concepts");
        File dir2 = new File(dir, subjectId + "");
        if (!dir2.exists()) {
            dir2.mkdir();
        }
        File file = new File(dir2, a.getId() + ".data");
        try {
            objectToFile(file.getAbsolutePath(), a);
            list.put(a.getId() + "", a);
        } catch (IOException ex) {
            getLogger(SubjectDaoFile.class.getName()).log(SEVERE, null, ex);
        }
    }

    /**
     *
     * @return
     */
    @Override
    public Collection<Concept> getAll() {
        if (list.isEmpty()) {
            File dir = new File(conf.getWorkingDir(), "concepts");
            File dir2 = new File(dir, "" + subjectId);

            if (!dir2.exists()) {
                dir2.mkdir();
            }
            List<String> subDir = asList(dir2.list());

            subDir.stream().map((d) -> new File(dir2, d)).map((ds) -> (Concept) fileToObject(ds.getAbsolutePath())).forEach((c) -> {
                list.put(c.getId() + "", c);
            });
        }
        return list.values();
    }

    /**
     *
     * @param a
     * @return
     */
    @Override
    public Concept get(int a) {

        Concept c = null;

        if (list == null) {
            getAll();
        }

        Iterator<Concept> iterator = list.values().iterator();
        int index = -1;

        while (index < a) {
            c = iterator.next();
            index++;
        }

        return c;

    }

    /**
     *
     * @param a
     */
    @Override
    public void delete(Concept a) {
        File dir = new File(conf.getWorkingDir(), "concepts");
        File dir2 = new File(dir, "" + subjectId);
        File file = new File(dir2, a.getId() + ".data");
        if (file.exists()) {
            file.delete();
            list.remove(a.getId() + "");
            a = null;
        }
    }

}
